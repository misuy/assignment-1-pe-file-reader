/// @file
/// @brief Module with functions to read PE files

#include "pe_file.h"

/// @brief Read PE file header (COFF File Header)
/// @param[in] file File to read from
/// @param[in] pe_header Pointer to structure that contains header data
/// @param[in] offset Offset to the header
/// @return True in case of success, false otherwise
bool read_pe_header(FILE* file, struct PEHeader* pe_header, uint32_t offset) {
    if (fseek(file, offset, SEEK_SET)) return false;
    if (!fread(pe_header, sizeof(struct PEHeader), 1, file)) return false;
    return true;
}

/// @brief Read headers of PE file sections
/// @param[in] file File to read from
/// @param[in] pe_section_header Pointer to array of structures that contains sections headers data
/// @param[in] offset Offset to the header of the first section
/// @param[in] number_of_sections Number of sections
/// @return True in case of success, false otherwise
bool read_pe_section_headers(FILE* file, struct PESectionHeader* pe_section_header, uint32_t offset, uint16_t number_of_sections) {
    if (fseek(file, offset, SEEK_SET)) return false;

    for (uint16_t i=0; i<number_of_sections; i++) {
        if (!fread(pe_section_header + i, sizeof(struct PESectionHeader), 1, file)) return false;
    }

    return true;
}

/// @brief Read PE file data
/// @param[in] file File to read from
/// @param[in] pe_file Pointer to structure that contains file data
/// @return True in case of success, false otherwise
bool read_pe_file(FILE* file, struct PEFile* pe_file) {
    if (fseek(file, MAGIC_OFFSET, SEEK_SET)) return false;
    if (!fread(&(pe_file->header_offset), sizeof(pe_file->header_offset), 1, file)) return false;
    if (!read_pe_header(file, &pe_file->pe_header, pe_file->header_offset)) return false;
    pe_file->pe_section_headers = malloc(sizeof(struct PESectionHeader) * pe_file->pe_header.number_of_sections);
    pe_file->sections_offset = pe_file->header_offset + sizeof(struct PEHeader) + pe_file->pe_header.size_of_optional_header;
    if (!read_pe_section_headers(file, pe_file->pe_section_headers, pe_file->sections_offset, pe_file->pe_header.number_of_sections)) return false;

    return true;
}
