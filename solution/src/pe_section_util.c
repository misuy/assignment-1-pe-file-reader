/// @file
/// @brief Module with functions for working with sections of PE files

#include "pe_section_util.h"

/// @brief Find section by name
/// @param[in] pe_section_headers Pointer to sections headers array
/// @param[in] number_of_sections Number of sections
/// @param[in] section_name Name of the section to search for
/// @param[in] section Pointer to structure that contains section data
/// @return True in case of success, false otherwise
bool get_section_by_name(struct PESectionHeader* pe_section_headers, uint16_t number_of_sections, char* section_name, struct Section* section) {
    for (uint16_t i=0; i<number_of_sections; i++) {
        if (strcmp((char*) pe_section_headers[i].name, section_name) == 0) {
            section->offset = pe_section_headers[i].pointer_to_raw_data;
            section->size = pe_section_headers[i].size_of_raw_data;
            section->data = malloc(section->size);
            return true;
        }
    }
    return false;
}

/// @brief Read section from file
/// @param[in] file File to read from
/// @param[in] section Pointer to structure that contains section data
/// @return True in case of success, false otherwise
bool read_section(FILE* file, struct Section* section) {
    if (fseek(file, section->offset, SEEK_SET)) return false;
    if (!fread(section->data, section->size, 1, file)) return false;
    return true;
}

/// @brief Write section to file
/// @param[in] file File to write to
/// @param[in] section Pointer to structure that contains section data
/// @return True in case of success, false otherwise
bool write_section(FILE* file, struct Section* section) {
    if (!fwrite(section->data, section->size, 1, file)) return false;
    return true;
}
