/// @file
/// @brief Library entry point

#include "section_extractor.h"

/// @brief Extract data from section of PE file (searching by name) to another file
/// @param[in] file PE file in which we'll search section
/// @param[in] output_file File to write section data
/// @param[in] section_name Section name to search for
/// @return True in case of success, false otherwise
bool extract_section(FILE* file, FILE* output_file, char* section_name) {
    struct PEFile* pe_file = malloc(sizeof(struct PEFile));
    if (!read_pe_file(file, pe_file)) return false;

    struct Section* section = malloc(sizeof(struct Section));
    if (!get_section_by_name(pe_file->pe_section_headers, pe_file->pe_header.number_of_sections, section_name, section)) return false;
    if (!read_section(file, section)) return false;
    if (!write_section(output_file, section)) return false;

    free(pe_file->pe_section_headers);
    free(pe_file);
    free(section->data);
    free(section);

    return true;
}
