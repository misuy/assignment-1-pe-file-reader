/// @file
/// @brief header for section_extractor.c

#ifndef _ASSIGNMENT_1_PE_FILE_READER_SECTION_EXTRACTOR_H_
#define _ASSIGNMENT_1_PE_FILE_READER_SECTION_EXTRACTOR_H_

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "pe_file.h"
#include "pe_section_util.h"

bool extract_section(FILE* file, FILE* output_file, char* section_name);

#endif
