/// @file
/// @brief header for pe_file.c


#ifndef _ASSIGNMENT_1_PE_FILE_READER_PE_FILE_H_
#define _ASSIGNMENT_1_PE_FILE_READER_PE_FILE_H_

#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

/// Offset to the offset to the PE signature
#define MAGIC_OFFSET 0x3c
/// Size of the section name
#define SECTION_NAME_SIZE 8


#ifdef _MSC_VER
#pragma pack(push, 1)
#endif

/// @brief Structure that contains PE file header data
struct
#ifdef __clang__
__attribute__((packed))
#endif
PEHeader {
    /// Magic value to identify PE format
    uint8_t magic[4];
    /// Type of target machine
    uint16_t machine;
    /// Number of sections in the file
    uint16_t number_of_sections;
    /// Time of creating the file
    uint32_t time_data_stamp;
    /// Offset of the symbol table
    uint32_t pointer_to_symbol_table;
    /// Number of entries in the symbol table
    uint32_t number_of_symbols;
    /// Size of the optional header
    uint16_t size_of_optional_header;
    /// Flags that indicates the attributes of the file
    uint16_t characteristics;
};


/// @brief Structure that contains PE file section header data
struct
#ifdef __clang__
__attribute__((packed))
#endif
PESectionHeader {
    /// Name of the section
    uint8_t name[SECTION_NAME_SIZE];
    /// Size of the section when loaded into memory
    uint32_t virtual_size;
    /// Offset to the section when loaded into memory
    uint32_t virtual_address;
    /// Size of the section in the file
    uint32_t size_of_raw_data;
    /// Offset to the section in the file
    uint32_t pointer_to_raw_data;
    /// Pointer to the beginning of relocation entries for the section
    uint32_t pointer_to_relocations;
    /// Pointer to the beginning of line-number entries for the section
    uint32_t pointer_to_linenumbers;
    /// Number of relocation entries for the section
    uint16_t number_of_relocations;
    /// Number of line-number entries for the section
    uint16_t number_of_linenumbers;
    /// Flags that describe the characteristics of the section
    uint32_t characteristics;
};


/// @brief Structure that contains PE file data
struct
#ifdef __clang__
    __attribute__((packed))
#endif
PEFile {
    /// Offset to the header of the file
    uint32_t header_offset;
    /// Offset to the first section of the file
    uint32_t sections_offset;
    /// Pointer to the structure that contains PE file header data
    struct PEHeader pe_header;
    /// Pointer to the structure that contains PE file section header data
    struct PESectionHeader* pe_section_headers;
};

#ifdef _MSC_VER
#pragma pack(pop)
#endif


bool read_pe_file(FILE* file, struct PEFile* pe_file);

#endif
