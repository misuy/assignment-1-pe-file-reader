/// @file
/// @brief header for pe_section_util.c

#ifndef _ASSIGNMENT_1_PE_FILE_READER_PE_SECTION_UTIL_H_
#define _ASSIGNMENT_1_PE_FILE_READER_PE_SECTION_UTIL_H_

#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "pe_file.h"

/// @brief Structure that contains PE file section data
struct Section {
    /// Offset to the section
    uint32_t offset;
    /// Size of the section
    uint32_t size;
    /// Section data
    uint8_t* data;
};

bool get_section_by_name(struct PESectionHeader* pe_section_headers, uint16_t number_of_sections, char* section_name, struct Section* section);
bool read_section(FILE* file, struct Section* section);
bool write_section(FILE* file, struct Section* section);

#endif
